import sqlite3

def create_db():
    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()

        cursor.execute("""DROP TABLE IF EXISTS employee;""")
        cursor.execute("""DROP TABLE IF EXISTS employer;""")
        cursor.execute("""DROP TABLE IF EXISTS user;""")
        cursor.execute("""DROP TABLE IF EXISTS job_offers;""")

        cursor.execute("""
        CREATE TABLE user (
            id INTEGER,
            email_address TEXT UNIQUE NOT NULL,
            password TEXT NOT NULL,
            PRIMARY KEY (id AUTOINCREMENT)
        );
        """)
        cursor.execute("""
        CREATE TABLE employee (
            id INTEGER,
            given_name TEXT NOT NULL,
            surname TEXT NOT NULL,
            birthdate TEXT,
            nationality TEXT,
            city TEXT,
            comment TEXT,
            PRIMARY KEY (id),
            FOREIGN KEY (id) REFERENCES user(id)
        );
        """)
        cursor.execute("""
        CREATE TABLE employer (
            id INTEGER,
            enterprise TEXT NOT NULL,
            department TEXT,
            siren_code TEXT,
            city TEXT NOT NULL,
            contact_surname TEXT,
            PRIMARY KEY (id),
            FOREIGN KEY (id) REFERENCES user(id)
        );
        """)
        cursor.execute("""
        CREATE TABLE job_offers (
            id INTEGER,
            id_employer INTEGER,
            title TEXT NOT NULL,
            posting_date TEXT,
            starting_date TEXT NOT NULL,
            duration TEXT,
            city TEXT,
            purpose TEXT NOT NULL,
            profile TEXT,
            PRIMARY KEY (id AUTOINCREMENT),
            FOREIGN KEY (id_employer) REFERENCES employer(id)
        );
        """)

        connection.commit()

if __name__ == "__main__":
    create_db()
