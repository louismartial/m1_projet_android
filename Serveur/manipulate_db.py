import bcrypt, datetime, sqlite3

def exists_credential(email_address, password):
    """returns -1 if the user doesn't exists, 0 if the password isn't corret, 1 if the user is an employee, 2 if the user is an employer"""

    user = select_user(email_address)
    if user is None:
        return -1
    elif not __matches_password(password, user[2]):
        return 0
    else:
        is_employer = select_employer(user[0])
        if is_employer is None:
            return 1
        else:
            return 2

def exists_user(email_address):
    return select_user(email_address) is not None

def insert_employee(email_address, password, given_name, surname, birthdate, nationality, city, comment):
    id = insert_user(email_address, password)

    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()
        cursor.execute("""INSERT INTO employee VALUES (?, ?, ?, ?, ?, ?, ?);""", (id, given_name, surname, birthdate, nationality, city, comment))
        connection.commit()

def insert_employer(email_address, password, enterprise, department, siren_code, city, contact_surname):
    id = insert_user(email_address, password)

    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()
        cursor.execute("""INSERT INTO employer VALUES (?, ?, ?, ?, ?, ?);""", (id, enterprise, department, siren_code, city, contact_surname))
        connection.commit()

def insert_job_offer(id_employer, title, starting_date, duration, city, purpose, profile):
    curent_date = datetime.datetime.now()
    formatted_date = curent_date.strftime("%d/%m/%Y")

    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()
        cursor.execute("""INSERT INTO job_offers VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?);""", (id_employer, title, formatted_date, starting_date, duration, city, purpose, profile))
        connection.commit()

def insert_user(email_address, password):
    with sqlite3.connect('gestion_interim.db') as connection:
        hashed_password = __hash_password(password)

        cursor = connection.cursor()
        cursor.execute("""INSERT INTO user VALUES (null, ?, ?);""", (email_address, hashed_password))
        connection.commit()

        return cursor.lastrowid

def select_employee(id):
    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM employee WHERE id = ?;", (id,))

    return cursor.fetchone()

def select_employer(id):
    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM employer WHERE id = ?;", (id,))

    return cursor.fetchone()

def select_employer_id(email_address):
    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT user.id FROM user INNER JOIN employer ON employer.id = user.id WHERE email_address = ?;", (email_address,))

    return cursor.fetchone()

def select_employer_job_offers(id_employer):
    with sqlite3.connect('gestion_interim.db') as connection:
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute("SELECT title, starting_date, city FROM job_offers WHERE id_employer = ?;", (id_employer,))

        results = cursor.fetchall()
        job_offers = []
        for row in results:
            job_offer = {
                'title': row['title'],
                'startingDate': row['starting_date'],
                'city': row['city']
            }
            job_offers.append(job_offer)

        return job_offers

def select_job_offer_summaries(keywords):
    with sqlite3.connect('gestion_interim.db') as connection:
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute("SELECT title, starting_date, city FROM job_offers WHERE title LIKE ?;", ('{}%'.format(keywords),))

        results = cursor.fetchall()
        job_offers = []
        for row in results:
            job_offer = {
                'title': row['title'],
                'startingDate': row['starting_date'],
                'city': row['city']
            }
            job_offers.append(job_offer)

        return job_offers

def select_job_offer_titles(keywords):
    with sqlite3.connect('gestion_interim.db') as connection:
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute("SELECT title FROM job_offers WHERE title LIKE ?;", ('{}%'.format(keywords),))

        results = cursor.fetchall()
        job_offers = []
        for row in results:
            job_offer = {
                'title': row['title']
            }
            job_offers.append(job_offer)

        return job_offers

def select_user(email_address):
    with sqlite3.connect('gestion_interim.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM user WHERE email_address = ?;", (email_address,))

    return cursor.fetchone()

def __hash_password(password):
    password = password.encode('utf-8')
    salt = bcrypt.gensalt()
    hashed_password = bcrypt.hashpw(password, salt)
    return hashed_password.decode('utf-8')

def __matches_password(password_1, password_2):
    return bcrypt.checkpw(password_1.encode('utf-8'), password_2.encode('utf-8'))
