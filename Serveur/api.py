from manipulate_db import exists_user, exists_credential, insert_employee, insert_employer, insert_job_offer, select_employer_id, select_employer_job_offers, select_job_offer_summaries, select_job_offer_titles
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/api/job_offer/title', methods=['GET'])
def job_offer_titles():
    keywords = request.args.get('keywords')
    if not keywords:
        return jsonify([])

    return jsonify(select_job_offer_titles(keywords))

@app.route('/api/job_offer/summary', methods=['GET'])
def job_offer_summary():
    keywords = request.args.get('keywords')
    if not keywords:
        return jsonify([])

    return jsonify(select_job_offer_summaries(keywords))

@app.route('/api/job_offer/summary_employer', methods=['GET'])
def job_offer_summary_employer():
    employer_email_address = request.args.get('employerEmailAddress')
    id_employer = select_employer_id(employer_email_address)

    return jsonify(select_employer_job_offers(id_employer[0]))

@app.route('/api/login', methods = ['POST'])
def login():
    message = request.get_json()

    email_address = message.get('emailAddress')
    password = message.get('password')

    match exists_credential(email_address, password):
        case -1:
            return jsonify({"message": "L'utilisateur n'existe pas."}), 404
        case 0:
            return jsonify({"message": "Le mot de passe ne correspond pas."}), 401
        case 1:
            return jsonify({"message": "Connexion réussie.", "userStatus": "EMPLOYEE"}), 200
        case 2:
            return jsonify({"message": "Connexion réussie.", "userStatus": "EMPLOYER"}), 200

@app.route('/api/publication/job_offer', methods=['POST'])
def publication_job_offer():
    message = request.get_json()

    employer_email_address = message.get('employerEmailAddress')
    title = message.get('title')
    starting_date = message.get('startingDate')
    duration = message.get('duration')
    city = message.get('city')
    purpose = message.get('purpose')
    profile = message.get('profile')

    id_employer = select_employer_id(employer_email_address)
    if id_employer is None:
        return jsonify({"message": "L'employeur n'existe pas"}), 404

    insert_job_offer(id_employer[0], title, starting_date, duration, city, purpose, profile)

    return jsonify({"message": "Offre d'emploi publiée avec succès."}), 201

@app.route('/api/registration/employee', methods = ['POST'])
def registration_employee():
    message = request.get_json()

    email_address = message.get('emailAddress')
    password = message.get('password')
    given_name = message.get('givenName')
    surname = message.get('surname')
    birthdate = message.get('birthdate')
    nationality = message.get('nationality')
    city = message.get('city')
    comment = message.get('comment')

    if exists_user(email_address):
        return jsonify({"message": "L'adresse électronique est déjà utilisée."}), 409

    insert_employee(email_address, password, given_name, surname, birthdate, nationality, city, comment)

    return jsonify({"message": "Compte créé avec succès."}), 201

@app.route('/api/registration/employer', methods = ['POST'])
def registration_employer():
    message = request.get_json()

    email_address = message.get('emailAddress')
    password = message.get('password')
    enterprise = message.get('enterprise')
    department = message.get('department')
    siren_code = message.get('sirenCode')
    city = message.get('city')
    contact_surname = message.get('contactSurname')

    if exists_user(email_address):
        return jsonify({"message": "L'adresse électronique est déjà utilisée."}), 409

    insert_employer(email_address, password, enterprise, department, siren_code, city, contact_surname)

    return jsonify({"message": "Compte créé avec succès."}), 201

if __name__ == '__main__':
    app.run(debug = True)
