package com.android.gestion_interim.network;

public class JobSummaryResponse {
    private String title;
    private String startingDate;
    private String city;

    public String getTitle() {
        return title;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public String getCity() {
        return city;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
