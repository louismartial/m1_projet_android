package com.android.gestion_interim.registration.employer_identity;

import java.util.List;

public interface EmployerIdentityView  {
    String getEnterprise();
    String getDepartment();
    String getSirenCode();
    String getCity();

    void setCitySuggestions(List<String> citySuggestions);
    void updateNextButtonState(boolean isEnabled);

    void passDataToActivity(String enterprise, String department, String siren, String city);
}