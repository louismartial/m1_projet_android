package com.android.gestion_interim.employer.publication.job_offer_details;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employer.publication.OnPublicationButtonListener;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class JobOfferDetailsFragment extends Fragment implements JobOfferDetailsView {
    private TextInputEditText titleEditText;
    private TextInputEditText startingDateEditText;
    private TextInputEditText durationEditText;
    private MaterialAutoCompleteTextView cityTextView;
    private MaterialButton nextButton;
    private OnPublicationButtonListener onPublicationButtonListener;
    private JobOfferDetailsController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnPublicationButtonListener) {
            onPublicationButtonListener = (OnPublicationButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnPublicationButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPublicationButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_offer_details, container, false);

        titleEditText = view.findViewById(R.id.edit_text_title);
        startingDateEditText = view.findViewById(R.id.edit_text_starting_date);
        durationEditText = view.findViewById(R.id.edit_text_duration);
        cityTextView = view.findViewById(R.id.text_view_city);
        nextButton = view.findViewById(R.id.button_next);

        controller = new JobOfferDetailsController(this);
        controller.loadCitySuggestions(getContext());

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                controller.updateNextButtonState();
            }
        };

        titleEditText.addTextChangedListener(textWatcher);
        startingDateEditText.addTextChangedListener(textWatcher);
        startingDateEditText.setOnClickListener(v -> controller.onDateEditTextClicked());
        nextButton.setOnClickListener(v -> controller.onNextButtonClick());

        return view;
    }

    @Override
    public String getTitle() {
        return Objects.requireNonNull(titleEditText.getText()).toString();
    }

    @Override
    public String getStartingDate() {
        return Objects.requireNonNull(startingDateEditText.getText()).toString();
    }

    @Override
    public String getDuration() {
        return Objects.requireNonNull(durationEditText.getText()).toString();
    }

    @Override
    public String getCity() {
        return Objects.requireNonNull(cityTextView.getText()).toString();
    }

    @Override
    public void setCitySuggestions(List<String> citiesSuggestions) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), com.google.android.material.R.layout.support_simple_spinner_dropdown_item, citiesSuggestions);
        cityTextView.setAdapter(adapter);
    }

    @Override
    public void showDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (datePicker, selectedYear, selectedMonth, selectedDay) -> {
            String date = selectedYear + "/" + (selectedMonth + 1)  + "/" + selectedDay;
            startingDateEditText.setText(date);
        }, year, month, day);

        datePickerDialog.show();
    }

    @Override
    public void updateNextButtonState(boolean isEnabled) {
        nextButton.setEnabled(isEnabled);
    }

    @Override
    public void passDataToActivity(String title, String startingDate, String duration, String city) {
        onPublicationButtonListener.onJobOfferDetailsButtonClick(title, startingDate, duration, city);
    }
}