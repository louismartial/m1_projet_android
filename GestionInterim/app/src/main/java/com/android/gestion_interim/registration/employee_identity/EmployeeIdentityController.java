package com.android.gestion_interim.registration.employee_identity;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class EmployeeIdentityController {
    private final EmployeeIdentityView view;

    public EmployeeIdentityController(EmployeeIdentityView view) {
        this.view = view;
    }

    public void loadCountrySuggestions(Context context) {
        List<String> countries = readCountriesFromCsv(context);

        view.setCountrySuggestions(countries);
    }

    public void onDateEditTextClicked() {
        view.showDatePickerDialog();
    }

    public void updateNextButtonState() {
        boolean isGivenNameFilled = !view.getGivenName().isEmpty();
        boolean isSurnameFilled = !view.getSurname().isEmpty();

        view.updateNextButtonState(isGivenNameFilled && isSurnameFilled);
    }

    public void onNextButtonClick() {
        String givenName = view.getGivenName();
        String surname = view.getSurname();
        String birthdate = view.getBirthdate();
        String nationality = view.getNationality();

        view.passDataToActivity(givenName, surname, birthdate, nationality);
    }

    private List<String> readCountriesFromCsv(Context context) {
        List<String> countries = new ArrayList<>();

        try (InputStream inputStream = context.getAssets().open("countries.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] columns = line.split(",");
                String countryName = columns[0];
                countries.add(countryName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return countries;
    }
}
