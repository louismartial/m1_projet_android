package com.android.gestion_interim.employer.publication.job_offer_details_extra;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employer.publication.OnPublicationButtonListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class JobOfferDetailsExtraFragment extends Fragment implements JobOfferDetailsExtraView {
    private TextInputEditText purposeEditText;
    private TextInputEditText profileEditText;
    private MaterialButton publicationButton;
    private OnPublicationButtonListener onPublicationButtonListener;
    private JobOfferDetailsExtraController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnPublicationButtonListener) {
            onPublicationButtonListener = (OnPublicationButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnPublicationButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPublicationButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_offer_details_extra, container, false);

        purposeEditText = view.findViewById(R.id.edit_text_purpose);
        profileEditText = view.findViewById(R.id.edit_text_profile);
        publicationButton = view.findViewById(R.id.button_publication);

        controller = new JobOfferDetailsExtraController(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                controller.updatePublicationButtonState();
            }
        };

        purposeEditText.addTextChangedListener(textWatcher);
        publicationButton.setOnClickListener(v -> controller.onPublicationButtonClick());

        return view;
    }

    @Override
    public String getPurpose() {
        return Objects.requireNonNull(purposeEditText.getText()).toString();
    }

    @Override
    public String getProfile() {
        return Objects.requireNonNull(profileEditText.getText()).toString();
    }

    @Override
    public void updatePublicationButtonState(boolean isEnabled) {
        publicationButton.setEnabled(isEnabled);
    }

    @Override
    public void passDataToActivity(String purpose, String profile) {
        onPublicationButtonListener.onJobOfferDetailsExtraButtonClick(purpose, profile);
    }
}