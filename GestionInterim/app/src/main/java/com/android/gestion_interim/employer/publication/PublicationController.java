package com.android.gestion_interim.employer.publication;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.gestion_interim.network.ApiService;
import com.android.gestion_interim.network.RetrofitClient;
import com.android.gestion_interim.utility.SharedPreferencesManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PublicationController {
    public final PublicationView view;
    public final PublicationJobOffer job;
    private final ApiService api;

    public PublicationController(PublicationView view) {
        this.view = view;
        this.job = new PublicationJobOffer();
        this.api = RetrofitClient.getRetrofitInstance().create(ApiService.class);
    }

    public void onJobOfferDetailsButtonClick(String title, String startingDate, String duration, String city) {
        job.setTitle(title);
        job.setStartingDate(startingDate);
        job.setDuration(duration);
        job.setCity(city);

        view.moveToJobOfferDetailsExtraFragment();
    }

    public void onJobOfferDetailsExtraButtonClick(Context context, String purpose, String profile) {
        job.setPurpose(purpose);
        job.setProfile(profile);

        job.setEmployerEmailAddress(SharedPreferencesManager.getUserEmailAddress(context));

        publishJobOffer();
    }

    private void publishJobOffer() {
        Call<Void> call = api.publishJobOffer(job);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.code() == 201) {
                    view.onPublicationSuccess();
                } else {
                    view.onPublicationFailure();
                }
            }
            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                Log.e("PublicationController", "ERREUR LORS DE LA PUBLICATION DE L'OFFRE D'EMPLOI", t);
                view.onPublicationFailure();
            }
        });
    }
}
