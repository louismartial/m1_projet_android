package com.android.gestion_interim.employer.publication;

public interface OnPublicationButtonListener {
    void onJobOfferDetailsButtonClick(String title, String startingDate, String duration, String city);
    void onJobOfferDetailsExtraButtonClick(String purpose, String profile);
}
