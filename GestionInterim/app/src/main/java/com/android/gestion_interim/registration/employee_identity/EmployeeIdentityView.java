package com.android.gestion_interim.registration.employee_identity;

import java.util.List;

public interface EmployeeIdentityView {
    String getGivenName();
    String getSurname();
    String getBirthdate();
    String getNationality();

    void setCountrySuggestions(List<String> countrySuggestions);
    void showDatePickerDialog();
    void updateNextButtonState(boolean isEnabled);

    void passDataToActivity(String givenName, String surname, String birthdate, String nationality);
}
