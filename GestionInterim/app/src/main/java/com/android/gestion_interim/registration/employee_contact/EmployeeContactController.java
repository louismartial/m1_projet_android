package com.android.gestion_interim.registration.employee_contact;

public class EmployeeContactController {
    private final EmployeeContactView view;

    public EmployeeContactController(EmployeeContactView view) {
        this.view = view;
    }

    public void updateRegistrationButtonState() {
        boolean isEmailFilled = !view.getEmailAddress().isEmpty();
        boolean isPasswordFilled = !view.getPassword().isEmpty();
        boolean isPasswordConfirmationFilled = !view.getPasswordConfirmation().isEmpty();

        view.updateRegistrationButtonState(isEmailFilled && isPasswordFilled && isPasswordConfirmationFilled);
    }

    public void onRegistrationButtonClick() {
        String emailAddress = view.getEmailAddress();
        String password = view.getPassword();
        String passwordConfirmation = view.getPasswordConfirmation();

        if (!password.equals(passwordConfirmation)) {
            view.onPasswordConfirmationError();
        }
        else {
            view.passDataToActivity(emailAddress, password);
        }
    }
}
