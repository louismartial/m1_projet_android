package com.android.gestion_interim.employer.job_offers;

import com.android.gestion_interim.network.JobSummaryResponse;

import java.util.List;

public interface JobOffersView {
    void onNewJobOfferButtonClick();

    void showJobOffers(List<JobSummaryResponse> jobOffers);
}
