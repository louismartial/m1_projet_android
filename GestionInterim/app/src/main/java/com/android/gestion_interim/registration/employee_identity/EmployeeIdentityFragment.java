package com.android.gestion_interim.registration.employee_identity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.gestion_interim.R;
import com.android.gestion_interim.registration.OnRegistrationButtonListener;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class EmployeeIdentityFragment extends Fragment implements EmployeeIdentityView {
    private TextInputEditText givenNameEditText;
    private TextInputEditText surnameEditText;
    private TextInputEditText birthDateEditText;
    private MaterialAutoCompleteTextView nationalityTextView;
    private MaterialButton nextButton;
    private OnRegistrationButtonListener onRegistrationButtonListener;
    private EmployeeIdentityController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnRegistrationButtonListener) {
            onRegistrationButtonListener = (OnRegistrationButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnRegistrationButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onRegistrationButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee_identity, container, false);

        givenNameEditText = view.findViewById(R.id.edit_text_given_name);
        surnameEditText = view.findViewById(R.id.edit_text_surname);
        birthDateEditText = view.findViewById(R.id.edit_text_birthdate);
        nationalityTextView = view.findViewById(R.id.text_view_nationality);
        nextButton = view.findViewById(R.id.button_next);

        controller = new EmployeeIdentityController(this);
        controller.loadCountrySuggestions(getContext());

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                controller.updateNextButtonState();
            }
        };

        givenNameEditText.addTextChangedListener(textWatcher);
        surnameEditText.addTextChangedListener(textWatcher);
        birthDateEditText.setOnClickListener(v -> controller.onDateEditTextClicked());
        nextButton.setOnClickListener(v -> controller.onNextButtonClick());

        return view;
    }

    @Override
    public String getGivenName() {
        return Objects.requireNonNull(givenNameEditText.getText()).toString();
    }

    @Override
    public String getSurname() {
        return Objects.requireNonNull(surnameEditText.getText()).toString();
    }

    @Override
    public String getBirthdate() {
        return Objects.requireNonNull(birthDateEditText.getText()).toString();
    }

    @Override
    public String getNationality() {
        return nationalityTextView.getText().toString();
    }

    @Override
    public void setCountrySuggestions(List<String> countriesSuggestions) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), com.google.android.material.R.layout.support_simple_spinner_dropdown_item, countriesSuggestions);
        nationalityTextView.setAdapter(adapter);
    }

    @Override
    public void showDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (datePicker, selectedYear, selectedMonth, selectedDay) -> {
            String date = selectedYear + "/" + (selectedMonth + 1)  + "/" + selectedDay;
            birthDateEditText.setText(date);
        }, year, month, day);

        datePickerDialog.show();
    }

    @Override
    public void updateNextButtonState(boolean isEnabled) {
        nextButton.setEnabled(isEnabled);
    }

    @Override
    public void passDataToActivity(String givenName, String surname, String birthdate, String nationality) {
        onRegistrationButtonListener.onEmployeeFirstButtonClick(givenName, surname, birthdate, nationality);
    }
}