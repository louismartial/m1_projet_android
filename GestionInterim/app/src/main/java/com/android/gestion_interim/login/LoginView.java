package com.android.gestion_interim.login;

public interface LoginView {
    String getEmailAddress();
    String getPassword();

    void clearPassword();
    void clearAll();

    void updateLoginButtonState(boolean isEnabled);

    void onEmailAddressError();
    void onPasswordError();
    void onLoginFailure();
    void onLoginSuccess(String userStatus);
}
