package com.android.gestion_interim.job_search;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.gestion_interim.R;

public class JobSearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_search);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_job_search, new JobSearchFragment()).commit();
    }
}