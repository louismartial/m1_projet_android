package com.android.gestion_interim.registration;

import java.io.Serializable;

public class RegistrationEmployee implements Serializable {
    private String emailAddress;
    private String password;
    private String givenName;
    private String surname;
    private String birthdate;
    private String nationality;
    private String city;
    private String comment;

    public RegistrationEmployee() {
        this.emailAddress = "";
        this.password = "";
        this.givenName = "";
        this.surname = "";
        this.birthdate = "";
        this.nationality = "";
        this.city = "";
        this.comment = "";
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSurname() {
        return surname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getNationality() {
        return nationality;
    }

    public String getCity() {
        return city;
    }

    public String getComment() {
        return comment;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
