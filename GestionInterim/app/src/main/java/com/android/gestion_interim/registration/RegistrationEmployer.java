package com.android.gestion_interim.registration;

import java.io.Serializable;

public class RegistrationEmployer implements Serializable {
    private String emailAddress;
    private String password;
    private String enterprise;
    private String department;
    private String sirenCode;
    private String city;
    private String contactSurname;

    public RegistrationEmployer() {
        this.emailAddress = "";
        this.password = "";
        this.enterprise = "";
        this.department = "";
        this.sirenCode = "";
        this.city = "";
        this.contactSurname = "";
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public String getDepartment() {
        return department;
    }

    public String getSirenCode() {
        return sirenCode;
    }

    public String getCity() {
        return city;
    }

    public String getContactSurname() {
        return contactSurname;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setSirenCode(String sirenCode) {
        this.sirenCode = sirenCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setContactSurname(String contactSurname) {
        this.contactSurname = contactSurname;
    }
}
