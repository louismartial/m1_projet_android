package com.android.gestion_interim.employer.job_offers;

import android.util.Log;

import androidx.annotation.NonNull;

import com.android.gestion_interim.network.ApiService;
import com.android.gestion_interim.network.JobSummaryResponse;
import com.android.gestion_interim.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobOffersController {
    private final JobOffersView view;
    private final ApiService api;

    public JobOffersController(JobOffersView view) {
        this.view = view;
        this.api = RetrofitClient.getRetrofitInstance().create(ApiService.class);
    }

    public void onNewJobOfferButtonClick() {
        view.onNewJobOfferButtonClick();
    }

    public void searchEmployerJobOffers(String employerEmailAddress) {
        Call<List<JobSummaryResponse>> call = api.jobEmployerSummarySearch(employerEmailAddress);
        call.enqueue(new Callback<List<JobSummaryResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<JobSummaryResponse>> call, @NonNull Response<List<JobSummaryResponse>> response) {
                if (response.isSuccessful()) {
                    view.showJobOffers(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<JobSummaryResponse>> call, @NonNull Throwable t) {
                Log.e("JobOffersController", "ERREUR LORS DE RÉCEPTION DES OFFRES D'EMPLOI", t);
            }
        });
    }
}
