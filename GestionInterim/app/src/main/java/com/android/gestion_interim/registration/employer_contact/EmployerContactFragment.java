package com.android.gestion_interim.registration.employer_contact;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.gestion_interim.R;
import com.android.gestion_interim.registration.OnRegistrationButtonListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class EmployerContactFragment  extends Fragment implements EmployerContactView {
    private TextInputEditText contactSurnameEditText;
    private TextInputEditText emailAddressEditText;
    private TextInputEditText passwordEditText;
    private TextInputEditText passwordConfirmationEditText;
    private MaterialButton registrationButton;
    private OnRegistrationButtonListener onRegistrationButtonListener;
    private EmployerContactController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnRegistrationButtonListener) {
            onRegistrationButtonListener = (OnRegistrationButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnRegistrationButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onRegistrationButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employer_contact, container, false);

        contactSurnameEditText = view.findViewById(R.id.edit_text_contact_surname);
        emailAddressEditText = view.findViewById(R.id.edit_text_email_address);
        passwordEditText = view.findViewById(R.id.edit_text_password);
        passwordConfirmationEditText = view.findViewById(R.id.edit_text_password_confirmation);
        registrationButton = view.findViewById(R.id.button_registration);

        controller = new EmployerContactController(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                controller.updateRegistrationButtonState();
            }
        };

        emailAddressEditText.addTextChangedListener(textWatcher);
        passwordEditText.addTextChangedListener(textWatcher);
        passwordConfirmationEditText.addTextChangedListener(textWatcher);
        registrationButton.setOnClickListener(v -> controller.onRegistrationButtonClick());

        return view;
    }

    @Override
    public String getContactSurame() {
        return Objects.requireNonNull(contactSurnameEditText.getText()).toString();
    }

    @Override
    public String getEmailAddress() {
        return Objects.requireNonNull(emailAddressEditText.getText()).toString();
    }

    @Override
    public String getPassword() {
        return Objects.requireNonNull(passwordEditText.getText()).toString();
    }

    @Override
    public String getPasswordConfirmation() {
        return Objects.requireNonNull(passwordConfirmationEditText.getText()).toString();
    }

    @Override
    public void onPasswordConfirmationError() {
        showAlertDialog(getString(R.string.password_error));
        passwordConfirmationEditText.setText("");
    }

    @Override
    public void updateRegistrationButtonState(boolean isEnabled) {
        registrationButton.setEnabled(isEnabled);
    }

    @Override
    public void passDataToActivity(String contactName, String emailAddress, String password) {
        onRegistrationButtonListener.onEmployerFinalButtonClick(contactName, emailAddress, password);
    }

    private void showAlertDialog(String message) {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AlertDialogRounded)
            .setTitle(getString(R.string.registration_error))
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok), (dialogInterface, i) -> {})
            .show();
    }
}