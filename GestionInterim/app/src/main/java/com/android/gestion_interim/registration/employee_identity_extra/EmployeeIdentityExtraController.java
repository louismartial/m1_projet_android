package com.android.gestion_interim.registration.employee_identity_extra;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class EmployeeIdentityExtraController {
    private final EmployeeIdentityExtraView view;

    public EmployeeIdentityExtraController(EmployeeIdentityExtraView view) {
        this.view = view;
    }

    public void loadCitySuggestions(Context context) {
        List<String> cities = readCitiesFromCsv(context);

        view.setCitySuggestions(cities);
    }

    public void updateNextButtonState() {
        boolean isCityFilled = !view.getCity().isEmpty();

        view.updateNextButtonState(isCityFilled);
    }

    public void onNextButtonClick() {
        String city = view.getCity();
        String comment = view.getComment();

        view.passDataToActivity(city, comment);
    }

    private List<String> readCitiesFromCsv(Context context) {
        List<String> cities = new ArrayList<>();

        try (InputStream inputStream = context.getAssets().open("cities.csv");
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] columns = line.split(",");
                String cityName = columns[0];
                cities.add(cityName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return cities;
    }
}