package com.android.gestion_interim.login;

public class LoginUser {
    private String emailAddress;
    private String password;

    public LoginUser() {
        this.emailAddress = "";
        this.password = "";
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}