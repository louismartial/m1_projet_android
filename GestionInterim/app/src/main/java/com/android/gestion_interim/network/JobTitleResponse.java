package com.android.gestion_interim.network;

public class JobTitleResponse {
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
