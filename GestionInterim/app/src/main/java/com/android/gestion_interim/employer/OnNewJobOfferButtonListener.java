package com.android.gestion_interim.employer;

public interface OnNewJobOfferButtonListener {
    void onNewJobOfferButtonClick();
}