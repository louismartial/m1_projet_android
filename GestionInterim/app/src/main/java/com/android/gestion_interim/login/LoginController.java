package com.android.gestion_interim.login;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.gestion_interim.network.LoginResponse;
import com.android.gestion_interim.network.RetrofitClient;
import com.android.gestion_interim.network.ApiService;
import com.android.gestion_interim.utility.SharedPreferencesManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginController {
    private final LoginView view;
    private final LoginUser user;
    private final ApiService api;

    public LoginController(LoginView view) {
        this.view = view;
        this.user = new LoginUser();
        this.api = RetrofitClient.getRetrofitInstance().create(ApiService.class);
    }

    public void updateLoginButtonState() {
        boolean isEmailFilled = !view.getEmailAddress().isEmpty();
        boolean isPasswordFilled = !view.getPassword().isEmpty();

        view.updateLoginButtonState(isEmailFilled && isPasswordFilled);
    }

    public void onLoginButtonClick(Context context) {
        user.setEmailAddress(view.getEmailAddress());
        user.setPassword(view.getPassword());
        loginUser(context);
    }

    private void loginUser(Context context) {
        Call<LoginResponse> call = api.loginUser(user);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    LoginResponse loginResponse = response.body();
                    String userStatus = loginResponse.getUserStatus();
                    SharedPreferencesManager.saveLoginState(context, true);
                    SharedPreferencesManager.saveUserStatus(context, userStatus);
                    SharedPreferencesManager.saveUserEmailAddress(context, user.getEmailAddress());
                    view.onLoginSuccess(userStatus);
                } else if (response.code() == 401) {
                    SharedPreferencesManager.saveLoginState(context, false);
                    view.onPasswordError();
                } else if (response.code() == 404) {
                    SharedPreferencesManager.saveLoginState(context, false);
                    view.onEmailAddressError();
                } else {
                    SharedPreferencesManager.saveLoginState(context, false);
                    view.onLoginFailure();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                Log.e("LoginController", "ERREUR LORS DE LA CONNEXION", t);
                SharedPreferencesManager.saveLoginState(context, false);
                view.onLoginFailure();
            }
        });
    }
}
