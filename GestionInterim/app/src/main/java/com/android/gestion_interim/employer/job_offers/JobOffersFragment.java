package com.android.gestion_interim.employer.job_offers;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employer.OnNewJobOfferButtonListener;
import com.android.gestion_interim.network.JobSummaryResponse;
import com.android.gestion_interim.utility.SharedPreferencesManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class JobOffersFragment extends Fragment implements JobOffersView {
    private OnNewJobOfferButtonListener onNewJobOfferButtonListener;
    private JobOffersController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnNewJobOfferButtonListener) {
            onNewJobOfferButtonListener = (OnNewJobOfferButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnNewJobOfferButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onNewJobOfferButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_offers, container, false);

        FloatingActionButton newJobOfferButton = view.findViewById(R.id.button_new_job_offer);

        controller = new JobOffersController(this);

        controller.searchEmployerJobOffers(SharedPreferencesManager.getUserEmailAddress(container.getContext()));
        newJobOfferButton.setOnClickListener(v -> controller.onNewJobOfferButtonClick());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        controller.searchEmployerJobOffers(SharedPreferencesManager.getUserEmailAddress(requireContext()));
    }

    @Override
    public void onNewJobOfferButtonClick() {
        onNewJobOfferButtonListener.onNewJobOfferButtonClick();
    }

    @Override
    public void showJobOffers(List<JobSummaryResponse> jobOffers) {
        RecyclerView recyclerView = getView().findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        JobSummaryAdapter adapter = new JobSummaryAdapter(jobOffers);
        recyclerView.setAdapter(adapter);
    }
}