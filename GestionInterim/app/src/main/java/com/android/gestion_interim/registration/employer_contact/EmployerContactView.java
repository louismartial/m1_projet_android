package com.android.gestion_interim.registration.employer_contact;

public interface EmployerContactView {
    String getContactSurame();
    String getEmailAddress();
    String getPassword();
    String getPasswordConfirmation();

    void onPasswordConfirmationError();
    void updateRegistrationButtonState(boolean isEnabled);

    void passDataToActivity(String contactName, String emailAddress, String password);
}
