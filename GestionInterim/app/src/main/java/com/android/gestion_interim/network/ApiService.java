package com.android.gestion_interim.network;

import com.android.gestion_interim.employer.publication.PublicationJobOffer;
import com.android.gestion_interim.login.LoginUser;
import com.android.gestion_interim.registration.RegistrationEmployee;
import com.android.gestion_interim.registration.RegistrationEmployer;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @GET("/api/job_offer/title")
    Call<List<JobTitleResponse>> jobTitleSearch(
        @Query("keywords") String keywords
    );

    @GET("/api/job_offer/summary")
    Call<List<JobSummaryResponse>> jobSummarySearch(
        @Query("keywords") String keywords
    );

    @GET("/api/job_offer/summary_employer")
    Call<List<JobSummaryResponse>> jobEmployerSummarySearch(
        @Query("employerEmailAddress") String employerEmailAddress
    );

    @POST("api/login")
    Call<LoginResponse> loginUser (
        @Body LoginUser loginUser
    );

    @POST("/api/publication/job_offer")
    Call<Void> publishJobOffer (
        @Body PublicationJobOffer publicationJobOffer
    );

    @POST("api/registration/employee")
    Call<Void> registerEmployee (
        @Body RegistrationEmployee registrationEmployee
    );

    @POST("api/registration/employer")
    Call<Void> registerEmployer (
        @Body RegistrationEmployer registrationEmployer
    );
}
