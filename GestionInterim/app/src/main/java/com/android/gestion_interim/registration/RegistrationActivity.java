package com.android.gestion_interim.registration;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employee.EmployeeActivity;
import com.android.gestion_interim.employer.EmployerActivity;
import com.android.gestion_interim.registration.employee_contact.EmployeeContactFragment;
import com.android.gestion_interim.registration.employee_identity.EmployeeIdentityFragment;
import com.android.gestion_interim.registration.employee_identity_extra.EmployeeIdentityExtraFragment;
import com.android.gestion_interim.registration.employer_contact.EmployerContactFragment;
import com.android.gestion_interim.registration.employer_identity.EmployerIdentityFragment;
import com.android.gestion_interim.registration.user_status.UserStatusFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class RegistrationActivity extends AppCompatActivity implements RegistrationView, OnRegistrationButtonListener {
    private RegistrationController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        controller = new RegistrationController(this);

        UserStatusFragment firstFragment = new UserStatusFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_registration, firstFragment).commit();
    }

    @Override
    public void moveToEmployeeIdentityFragment() {
        EmployeeIdentityFragment fragment = new EmployeeIdentityFragment();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.fragment_registration, fragment).addToBackStack(null).commit();
    }

    @Override
    public void moveToEmployeeIdentityExtraFragment() {
        EmployeeIdentityExtraFragment fragment = new EmployeeIdentityExtraFragment();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.fragment_registration, fragment).addToBackStack(null).commit();
    }

    @Override
    public void moveToEmployeeContactFragment() {
        EmployeeContactFragment fragment = new EmployeeContactFragment();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.fragment_registration, fragment).addToBackStack(null).commit();
    }

    @Override
    public void moveToEmployerIdentityFragment() {
        EmployerIdentityFragment fragment = new EmployerIdentityFragment();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.fragment_registration, fragment).addToBackStack(null).commit();
    }

    @Override
    public void moveToEmployerContactFragment() {
        EmployerContactFragment fragment = new EmployerContactFragment();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.fragment_registration, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onExistingEmailAddress() {
        showAlertDialog(getString(R.string.email_address_error_2));
    }

    @Override
    public void onRegistrationFailure() {
        showAlertDialog(getString(R.string.server_error));
    }

    @Override
    public void onRegistrationSuccess(String userStatus) {
        if (userStatus.equals("EMPLOYEE")) {
            Intent intent = new Intent(this, EmployeeActivity.class);
            setResult(1, intent);
            startActivity(intent);
        } else if (userStatus.equals("EMPLOYER")) {
            Intent intent = new Intent(this, EmployerActivity.class);
            setResult(1, intent);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onUserStatusButtonClick(String userStatus) {
        controller.onUserStatusButtonClick(userStatus);
    }

    @Override
    public void onEmployeeFirstButtonClick(String givenName, String surname, String birthdate, String nationality) {
        controller.onEmployeeFirstButtonClick(givenName, surname, birthdate, nationality);
    }

    @Override
    public void onEmployeeSecondButtonClick(String city, String comment) {
        controller.onEmployeeSecondButtonClick(city, comment);
    }

    @Override
    public void onEmployeeFinalButtonClick(String emailAddress, String password) {
        controller.onEmployeeFinalButtonClick(getApplicationContext(), emailAddress, password);
    }

    @Override
    public void onEmployerFirstButtonClick(String enterprise, String department, String sirenCode, String city) {
        controller.onEmployerFirstButtonClick(enterprise, department, sirenCode, city);
    }

    @Override
    public void onEmployerFinalButtonClick(String contactName, String emailAddress, String password) {
        controller.onEmployerFinalButtonClick(getApplicationContext(), contactName, emailAddress, password);
    }

    private void showAlertDialog(String message) {
        new MaterialAlertDialogBuilder(RegistrationActivity.this, R.style.AlertDialogRounded)
            .setTitle(getString(R.string.registration_error))
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok), (dialogInterface, i) -> {})
            .show();
    }
}