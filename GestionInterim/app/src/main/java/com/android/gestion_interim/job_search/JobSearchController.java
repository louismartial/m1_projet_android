package com.android.gestion_interim.job_search;

import android.util.Log;

import androidx.annotation.NonNull;

import com.android.gestion_interim.network.ApiService;
import com.android.gestion_interim.network.JobSummaryResponse;
import com.android.gestion_interim.network.JobTitleResponse;
import com.android.gestion_interim.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobSearchController {
    private final JobSearchView view;
    private final ApiService api;

    public JobSearchController(JobSearchView view) {
        this.view = view;
        this.api = RetrofitClient.getRetrofitInstance().create(ApiService.class);
    }

    public void searchJobOffers(String keywords) {
        api.jobSummarySearch(keywords).enqueue(new Callback<List<JobSummaryResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<JobSummaryResponse>> call, @NonNull Response<List<JobSummaryResponse>> response) {
                if (response.isSuccessful()) {
                    view.showSummaryJobs(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<JobSummaryResponse>> call, @NonNull Throwable t) {
                Log.e("JobSearchController", "ERREUR LORS DE RÉCEPTION DES OFFRES D'EMPLOI", t);
            }
        });
    }

    public void searchTitleJobs(String keywords) {
        api.jobTitleSearch(keywords).enqueue(new Callback<List<JobTitleResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<JobTitleResponse>> call, @NonNull Response<List<JobTitleResponse>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.showTitleJobs(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<JobTitleResponse>> call, @NonNull Throwable t) {}
        });
    }

    public void clearSearch() {
        view.clearJobs();
    }
}