package com.android.gestion_interim.network;

public class LoginResponse {
    private String message;
    private String userStatus;

    public String getMessage() {
        return message;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
}
