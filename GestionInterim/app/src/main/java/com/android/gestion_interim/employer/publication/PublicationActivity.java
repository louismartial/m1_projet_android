package com.android.gestion_interim.employer.publication;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employer.publication.job_offer_details.JobOfferDetailsFragment;
import com.android.gestion_interim.employer.publication.job_offer_details_extra.JobOfferDetailsExtraFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class PublicationActivity extends AppCompatActivity implements PublicationView, OnPublicationButtonListener {
    private PublicationController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publication);

        controller = new PublicationController(this);

        JobOfferDetailsFragment firstFragment = new JobOfferDetailsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_publication, firstFragment).commit();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_to_bottom);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void moveToJobOfferDetailsExtraFragment() {
        JobOfferDetailsExtraFragment fragment = new JobOfferDetailsExtraFragment();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.fragment_publication, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onPublicationSuccess() {
        showAlertDialog(getString(R.string.publication_success));
    }

    @Override
    public void onPublicationFailure() {
        showAlertDialog(getString(R.string.publication_error));
    }

    @Override
    public void onJobOfferDetailsButtonClick(String title, String startingDate, String duration, String city) {
        controller.onJobOfferDetailsButtonClick(title, startingDate, duration, city);
    }

    @Override
    public void onJobOfferDetailsExtraButtonClick(String purpose, String profile) {
        controller.onJobOfferDetailsExtraButtonClick(getApplicationContext(), purpose, profile);
    }

    private void showAlertDialog(String title) {
        new MaterialAlertDialogBuilder(PublicationActivity.this, R.style.AlertDialogRounded)
            .setMessage(title)
            .setPositiveButton(getString(R.string.ok), (dialogInterface, i) -> {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_to_bottom);
            })
            .show();
    }
}