package com.android.gestion_interim.employer.publication.job_offer_details_extra;

public class JobOfferDetailsExtraController {
    private final JobOfferDetailsExtraView view;

    public JobOfferDetailsExtraController(JobOfferDetailsExtraView view) {
        this.view = view;
    }

    public void updatePublicationButtonState() {
        boolean isPurposeFilled = !view.getPurpose().isEmpty();

        view.updatePublicationButtonState(isPurposeFilled);
    }

    public void onPublicationButtonClick() {
        String purpose = view.getPurpose();
        String profile = view.getProfile();

        view.passDataToActivity(purpose, profile);
    }
}
