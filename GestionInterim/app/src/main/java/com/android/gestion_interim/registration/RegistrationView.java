package com.android.gestion_interim.registration;

public interface RegistrationView {
    void moveToEmployeeIdentityFragment();
    void moveToEmployeeIdentityExtraFragment();
    void moveToEmployeeContactFragment();

    void moveToEmployerIdentityFragment();
    void moveToEmployerContactFragment();

    void onExistingEmailAddress();
    void onRegistrationFailure();
    void onRegistrationSuccess(String userStatus);
}