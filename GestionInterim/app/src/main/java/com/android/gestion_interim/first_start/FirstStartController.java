package com.android.gestion_interim.first_start;

public class FirstStartController {
    private final FirstStartView view;

    public FirstStartController(FirstStartView view) {
        this.view = view;
    }

    public void onSearchCardViewClicked() {
        view.navigateToJobSearchActivity();
    }

    public void onLoginButtonClicked() {
        view.navigateToLoginActivity();
    }

    public void onRegistrationButtonClicked() {
        view.navigateToRegistrationActivity();
    }
}
