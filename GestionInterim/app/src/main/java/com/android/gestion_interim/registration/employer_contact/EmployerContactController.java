package com.android.gestion_interim.registration.employer_contact;

public class EmployerContactController {
    private final EmployerContactView view;

    public EmployerContactController(EmployerContactView view) {
        this.view = view;
    }

    public void updateRegistrationButtonState() {
        boolean isEmailFilled = !view.getEmailAddress().isEmpty();
        boolean isPasswordFilled = !view.getPassword().isEmpty();
        boolean isPasswordConfirmationFilled = !view.getPasswordConfirmation().isEmpty();

        view.updateRegistrationButtonState(isEmailFilled && isPasswordFilled && isPasswordConfirmationFilled);
    }

    public void onRegistrationButtonClick() {
        String contactSurname = view.getContactSurame();
        String emailAddress = view.getEmailAddress();
        String password = view.getPassword();
        String passwordConfirmation = view.getPasswordConfirmation();

        if (!password.equals(passwordConfirmation)) {
            view.onPasswordConfirmationError();
        }
        else {
            view.passDataToActivity(contactSurname, emailAddress, password);
        }
    }
}
