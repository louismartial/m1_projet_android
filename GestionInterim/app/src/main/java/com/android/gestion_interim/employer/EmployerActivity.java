package com.android.gestion_interim.employer;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employer.job_applications.JobApplicationsFragment;
import com.android.gestion_interim.employer.job_offers.JobOffersFragment;

import com.android.gestion_interim.employer.publication.PublicationActivity;
import com.android.gestion_interim.first_start.FirstStartActivity;
import com.android.gestion_interim.utility.SharedPreferencesManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class EmployerActivity extends AppCompatActivity implements OnNewJobOfferButtonListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer);

        BottomNavigationView menu = findViewById(R.id.menu_employer);

        menu.setSelectedItemId(R.id.item_job_offers);
        menu.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.item_job_applications) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_employer, new JobApplicationsFragment()).commit();
            } else if (item.getItemId() == R.id.item_job_offers) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_employer, new JobOffersFragment()).commit();
            } else if (item.getItemId() == R.id.item_logout) {
                logout();
            }

            return true;
        });

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_employer, new JobOffersFragment()).commit();
    }

    @Override
    public void onNewJobOfferButtonClick() {
        Intent intent = new Intent(this, PublicationActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_from_bottom, R.anim.stay);
    }

    private void logout() {
        SharedPreferencesManager.saveUserEmailAddress(this, "");
        SharedPreferencesManager.saveLoginState(this, false);
        Intent intent = new Intent(this, FirstStartActivity.class);
        startActivity(intent);
        finish();
    }
}