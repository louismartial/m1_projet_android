package com.android.gestion_interim.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.appcompat.app.AppCompatActivity;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employee.EmployeeActivity;
import com.android.gestion_interim.employer.EmployerActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private TextInputEditText emailAddressEditText;
    private TextInputEditText passwordEditText;
    private MaterialButton loginButton;
    private LoginController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailAddressEditText = findViewById(R.id.edit_text_email_address);
        passwordEditText = findViewById(R.id.edit_text_password);
        loginButton = findViewById(R.id.button_login);

        controller = new LoginController(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                controller.updateLoginButtonState();
            }
        };

        emailAddressEditText.addTextChangedListener(textWatcher);
        passwordEditText.addTextChangedListener(textWatcher);
        loginButton.setOnClickListener(view -> controller.onLoginButtonClick(getApplicationContext()));
    }

    @Override
    public String getEmailAddress() {
        return Objects.requireNonNull(emailAddressEditText.getText()).toString();
    }

    @Override
    public String getPassword() {
        return Objects.requireNonNull(passwordEditText.getText()).toString();
    }

    @Override
    public void clearPassword() {
        passwordEditText.setText("");
    }

    @Override
    public void clearAll() {
        emailAddressEditText.setText("");
        passwordEditText.setText("");
    }

    @Override
    public void updateLoginButtonState(boolean isEnabled) {
        loginButton.setEnabled(isEnabled);
    }

    @Override
    public void onEmailAddressError() {
        showAlertDialog(getString(R.string.email_address_error_1));
        clearAll();
    }

    @Override
    public void onPasswordError() {
        showAlertDialog(getString(R.string.password_error));
        clearPassword();
    }

    @Override
    public void onLoginFailure() {
        showAlertDialog(getString(R.string.server_error));
        clearAll();
    }

    @Override
    public void onLoginSuccess(String userStatus) {
        if (userStatus.equals("EMPLOYEE")) {
            Intent intent = new Intent(this, EmployeeActivity.class);
            setResult(1, intent);
            startActivity(intent);
        } else if (userStatus.equals("EMPLOYER")) {
            Intent intent = new Intent(this, EmployerActivity.class);
            setResult(1, intent);
            startActivity(intent);
        }
        finish();
    }

    private void showAlertDialog(String message) {
        new MaterialAlertDialogBuilder(LoginActivity.this, R.style.AlertDialogRounded)
            .setTitle(getString(R.string.connexion_error))
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok), (dialogInterface, i) -> {})
            .show();
    }
}