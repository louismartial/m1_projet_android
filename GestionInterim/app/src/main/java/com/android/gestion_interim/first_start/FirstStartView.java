package com.android.gestion_interim.first_start;

public interface FirstStartView {
    void navigateToJobSearchActivity();
    void navigateToLoginActivity();
    void navigateToRegistrationActivity();
}
