package com.android.gestion_interim.registration;

public interface OnRegistrationButtonListener {
    void onUserStatusButtonClick(String userStatus);

    void onEmployeeFirstButtonClick(String givenName, String surname, String birthdate, String nationality);
    void onEmployeeSecondButtonClick(String city, String comment);
    void onEmployeeFinalButtonClick(String emailAddress, String password);

    void onEmployerFirstButtonClick(String enterprise, String department, String sirenCode, String city);
    void onEmployerFinalButtonClick(String contactSurname, String emailAddress, String password);
}
