package com.android.gestion_interim.employer.publication.job_offer_details;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JobOfferDetailsController {
    private final JobOfferDetailsView view;

    public JobOfferDetailsController(JobOfferDetailsView view) {
        this.view = view;
    }

    public void loadCitySuggestions(Context context) {
        List<String> cities = readCitiesFromCsv(context);

        view.setCitySuggestions(cities);
    }

    public void onDateEditTextClicked() {
        view.showDatePickerDialog();
    }

    public void updateNextButtonState() {
        boolean isTitleFilled = !view.getTitle().isEmpty();
        boolean isStartingDateFilled = !view.getStartingDate().isEmpty();

        view.updateNextButtonState(isTitleFilled && isStartingDateFilled);
    }

    public void onNextButtonClick() {
        String title = view.getTitle();
        String startingDate = view.getStartingDate();
        String duration = view.getDuration();
        String city = view.getCity();

        view.passDataToActivity(title, startingDate, duration, city);
    }

    private List<String> readCitiesFromCsv(Context context) {
        List<String> cities = new ArrayList<>();

        try (InputStream inputStream = context.getAssets().open("cities.csv");
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] columns = line.split(",");
                String cityName = columns[0];
                cities.add(cityName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return cities;
    }
}
