package com.android.gestion_interim.registration.employee_contact;

public interface EmployeeContactView {
    String getEmailAddress();
    String getPassword();
    String getPasswordConfirmation();

    void updateRegistrationButtonState(boolean isEnabled);
    void onPasswordConfirmationError();

    void passDataToActivity(String emailAddress, String password);
}
