package com.android.gestion_interim.employer.publication;

import java.io.Serializable;

public class PublicationJobOffer implements Serializable {
    private String employerEmailAddress;
    private String title;
    private String startingDate;
    private String duration;
    private String city;
    private String purpose;
    private String profile;

    public PublicationJobOffer() {
        this.title = "";
        this.startingDate = "";
        this.duration = "";
        this.city = "";
        this.purpose = "";
        this.profile = "";
    }

    public String getEmployerEmailAddress() {
        return employerEmailAddress;
    }

    public String getTitle() {
        return title;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public String getDuration() {
        return duration;
    }

    public String getCity() {
        return city;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getProfile() {
        return profile;
    }

    public void setEmployerEmailAddress(String employerEmailAddress) {
        this.employerEmailAddress = employerEmailAddress;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
