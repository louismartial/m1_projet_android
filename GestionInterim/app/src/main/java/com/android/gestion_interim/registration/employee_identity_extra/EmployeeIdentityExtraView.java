package com.android.gestion_interim.registration.employee_identity_extra;

import java.util.List;

public interface EmployeeIdentityExtraView {
    String getCity();
    String getComment();

    void setCitySuggestions(List<String> citySuggestions);
    void updateNextButtonState(boolean isEnabled);

    void passDataToActivity(String city, String comment);
}
