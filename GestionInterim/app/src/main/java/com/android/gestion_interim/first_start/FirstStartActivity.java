package com.android.gestion_interim.first_start;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employee.EmployeeActivity;
import com.android.gestion_interim.employer.EmployerActivity;
import com.android.gestion_interim.job_search.JobSearchActivity;
import com.android.gestion_interim.login.LoginActivity;
import com.android.gestion_interim.registration.RegistrationActivity;
import com.android.gestion_interim.utility.SharedPreferencesManager;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

public class FirstStartActivity extends AppCompatActivity implements FirstStartView {
    private ActivityResultLauncher<Intent> activityLauncher;
    private FirstStartController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean isLoggedIn = SharedPreferencesManager.getLoginState(this);
        if (isLoggedIn) {
            String userStatus = SharedPreferencesManager.getUserStatus(this);
            if (userStatus.equals("EMPLOYEE")) {
                Intent intent = new Intent(this, EmployeeActivity.class);
                startActivity(intent);
            } else if (userStatus.equals("EMPLOYER")) {
                Intent intent = new Intent(this, EmployerActivity.class);
                startActivity(intent);
            }
            finish();
        }

        activityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if (result.getResultCode() == 1) {
                    finish();
                }
            }
        );

        setContentView(R.layout.activity_first_start);

        MaterialButton loginButton = findViewById(R.id.button_login);
        MaterialButton registrationButton = findViewById(R.id.button_registration);
        MaterialCardView searchCardView = findViewById(R.id.card_view_search);

        controller = new FirstStartController(this);

        searchCardView.setOnClickListener(view ->controller.onSearchCardViewClicked());
        loginButton.setOnClickListener(view -> controller.onLoginButtonClicked());
        registrationButton.setOnClickListener(view -> controller.onRegistrationButtonClicked());
    }

    @Override
    public void navigateToJobSearchActivity() {
        Intent intent = new Intent(this, JobSearchActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        activityLauncher.launch(intent);
    }

    @Override
    public void navigateToRegistrationActivity() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        activityLauncher.launch(intent);
    }
}