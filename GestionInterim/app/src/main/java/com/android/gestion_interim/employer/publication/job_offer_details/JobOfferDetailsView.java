package com.android.gestion_interim.employer.publication.job_offer_details;

import java.util.List;

public interface JobOfferDetailsView {
    String getTitle();
    String getStartingDate();
    String getDuration();
    String getCity();

    void setCitySuggestions(List<String> citySuggestions);
    void showDatePickerDialog();
    void updateNextButtonState(boolean isEnabled);

    void passDataToActivity(String title, String startingDate, String duration, String city);
}
