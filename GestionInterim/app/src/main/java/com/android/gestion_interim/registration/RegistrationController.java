package com.android.gestion_interim.registration;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.gestion_interim.network.RetrofitClient;
import com.android.gestion_interim.network.ApiService;
import com.android.gestion_interim.utility.SharedPreferencesManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationController {
    private final RegistrationView view;
    private final RegistrationEmployee employee;
    private final RegistrationEmployer employer;
    private final ApiService api;

    public RegistrationController(RegistrationView view) {
        this.view = view;
        this.employee = new RegistrationEmployee();
        this.employer = new RegistrationEmployer();
        this.api = RetrofitClient.getRetrofitInstance().create(ApiService.class);
    }

    public void onUserStatusButtonClick(String userStatus) {
        if (userStatus.matches("Je recherche un intérim")) {
            view.moveToEmployeeIdentityFragment();
        }
        else if (userStatus.matches("Je suis employeur")) {
            view.moveToEmployerIdentityFragment();
        }
    }

    public void onEmployeeFirstButtonClick(String givenName, String surname, String birthdate, String nationality) {
        employee.setGivenName(givenName);
        employee.setSurname(surname);
        employee.setBirthdate(birthdate);
        employee.setNationality(nationality);

        view.moveToEmployeeIdentityExtraFragment();
    }

    public void onEmployeeSecondButtonClick(String city, String comment) {
        employee.setCity(city);
        employee.setComment(comment);

        view.moveToEmployeeContactFragment();
    }

    public void onEmployeeFinalButtonClick(Context context, String emailAddress, String password) {
        employee.setEmailAddress(emailAddress);
        employee.setPassword(password);

        registerUser(context, emailAddress, "EMPLOYEE");
    }

    public void onEmployerFirstButtonClick(String enterprise, String department, String sirenCode, String city) {
        employer.setEnterprise(enterprise);
        employer.setDepartment(department);
        employer.setSirenCode(sirenCode);
        employer.setCity(city);

        view.moveToEmployerContactFragment();
    }

    public void onEmployerFinalButtonClick(Context context, String contactSurname, String emailAddress, String password) {
        employer.setContactSurname(contactSurname);
        employer.setEmailAddress(emailAddress);
        employer.setPassword(password);

        registerUser(context, emailAddress, "EMPLOYER");
    }

    private void registerUser(Context context, String userEmailAddress, String userStatus) {
        Call<Void> call;
        if (userStatus.matches("EMPLOYEE")) {
            call = api.registerEmployee(employee);
        } else if (userStatus.matches("EMPLOYER")) {
            call = api.registerEmployer(employer);
        } else {
            call = null;
        }

        if (call != null) {
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                    if (response.code() == 201) {
                        SharedPreferencesManager.saveLoginState(context, true);
                        SharedPreferencesManager.saveUserStatus(context, userStatus);
                        SharedPreferencesManager.saveUserEmailAddress(context, userEmailAddress);
                        view.onRegistrationSuccess(userStatus);
                    } else if (response.code() == 409) {
                        SharedPreferencesManager.saveLoginState(context, false);
                        view.onExistingEmailAddress();
                    } else {
                        SharedPreferencesManager.saveLoginState(context, false);
                        view.onRegistrationFailure();
                    }
                }
                @Override
                public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                    Log.e("RegistrationController", "ERREUR LORS DE LA CRÉATION DU COMPTE", t);
                    SharedPreferencesManager.saveLoginState(context, false);
                    view.onRegistrationFailure();
                }
            });
        }
    }
}
