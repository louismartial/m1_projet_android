package com.android.gestion_interim.registration.employee_identity_extra;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.gestion_interim.R;
import com.android.gestion_interim.registration.OnRegistrationButtonListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Objects;

public class EmployeeIdentityExtraFragment extends Fragment implements EmployeeIdentityExtraView {
    private MaterialAutoCompleteTextView cityTextView;
    private TextInputEditText commentEditText;
    MaterialButton nextButton;
    private OnRegistrationButtonListener onRegistrationButtonListener;
    private EmployeeIdentityExtraController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnRegistrationButtonListener) {
            onRegistrationButtonListener = (OnRegistrationButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnRegistrationButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onRegistrationButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_employee_identity_extra, container, false);

        cityTextView = view.findViewById(R.id.text_view_city);
        commentEditText = view.findViewById(R.id.edit_text_comment);
        nextButton = view.findViewById(R.id.button_next);

        controller = new EmployeeIdentityExtraController(this);
        controller.loadCitySuggestions(getContext());

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                controller.updateNextButtonState();
            }
        };

        nextButton.setOnClickListener(v -> controller.onNextButtonClick());

        return view;
    }

    @Override
    public String getCity() {
        return cityTextView.getText().toString();
    }

    @Override
    public String getComment() {
        return Objects.requireNonNull(commentEditText.getText()).toString();
    }

    @Override
    public void setCitySuggestions(List<String> citySuggestions) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), com.google.android.material.R.layout.support_simple_spinner_dropdown_item, citySuggestions);
        cityTextView.setAdapter(adapter);
    }

    @Override
    public void updateNextButtonState(boolean isEnabled) {
        nextButton.setEnabled(isEnabled);
    }

    @Override
    public void passDataToActivity(String city, String comment) {
        onRegistrationButtonListener.onEmployeeSecondButtonClick(city, comment);
    }
}