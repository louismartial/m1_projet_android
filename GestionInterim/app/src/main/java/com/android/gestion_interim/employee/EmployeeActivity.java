package com.android.gestion_interim.employee;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employer.job_applications.JobApplicationsFragment;
import com.android.gestion_interim.first_start.FirstStartActivity;
import com.android.gestion_interim.job_search.JobSearchFragment;
import com.android.gestion_interim.utility.SharedPreferencesManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class EmployeeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);

        BottomNavigationView menu = findViewById(R.id.menu_employee);

        menu.setSelectedItemId(R.id.item_job_search);
        menu.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.item_job_applications) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_employee, new JobApplicationsFragment()).commit();
            } else if (item.getItemId() == R.id.item_job_search) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_employee, new JobSearchFragment()).commit();
            } else if (item.getItemId() == R.id.item_logout) {
                logout();
            }

            return true;
        });

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_employee, new JobSearchFragment()).commit();
    }

    private void logout() {
        SharedPreferencesManager.saveUserEmailAddress(this, "");
        SharedPreferencesManager.saveLoginState(this, false);
        Intent intent = new Intent(this, FirstStartActivity.class);
        startActivity(intent);
        finish();
    }
}