package com.android.gestion_interim.registration.employer_identity;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class EmployerIdentityController {
    private final EmployerIdentityView view;

    public EmployerIdentityController(EmployerIdentityView view) {
        this.view = view;
    }

    public void loadCitySuggestions(Context context) {
        List<String> cities = readCitiesFromCsv(context);

        view.setCitySuggestions(cities);
    }

    public void updateNextButtonState() {
        boolean isEnterpriseFilled = !view.getEnterprise().isEmpty();
        boolean isCityFilled = !view.getCity().isEmpty();

        view.updateNextButtonState(isEnterpriseFilled && isCityFilled);
    }

    public void onNextButtonClick() {
        String enterprise = view.getEnterprise();
        String department = view.getDepartment();
        String sirenCode = view.getSirenCode();
        String city = view.getCity();

        view.passDataToActivity(enterprise, department, sirenCode, city);
    }

    private List<String> readCitiesFromCsv(Context context) {
        List<String> cities = new ArrayList<>();

        try (InputStream inputStream = context.getAssets().open("cities.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] columns = line.split(",");
                String cityName = columns[0];
                cities.add(cityName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return cities;
    }
}
