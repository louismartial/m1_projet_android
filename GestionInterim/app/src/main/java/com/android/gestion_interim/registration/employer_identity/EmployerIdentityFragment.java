package com.android.gestion_interim.registration.employer_identity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.gestion_interim.R;
import com.android.gestion_interim.registration.OnRegistrationButtonListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Objects;

public class EmployerIdentityFragment extends Fragment implements EmployerIdentityView {
    private TextInputEditText entepriseEditText;
    private TextInputEditText departmentEditText;
    private TextInputEditText sirenCodeEditText;
    private MaterialAutoCompleteTextView cityTextView;
    private MaterialButton nextButton;
    private OnRegistrationButtonListener onRegistrationButtonListener;
    private EmployerIdentityController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnRegistrationButtonListener) {
            onRegistrationButtonListener = (OnRegistrationButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnRegistrationButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onRegistrationButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employer_identity, container, false);

        entepriseEditText = view.findViewById(R.id.edit_text_enterprise);
        departmentEditText = view.findViewById(R.id.edit_text_department);
        sirenCodeEditText = view.findViewById(R.id.edit_text_siren_code);
        cityTextView = view.findViewById(R.id.text_view_city);
        nextButton = view.findViewById(R.id.button_next);

        controller = new EmployerIdentityController(this);
        controller.loadCitySuggestions(getContext());

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                controller.updateNextButtonState();
            }
        };

        entepriseEditText.addTextChangedListener(textWatcher);
        cityTextView.addTextChangedListener(textWatcher);
        nextButton.setOnClickListener(v -> controller.onNextButtonClick());

        return view;
    }

    @Override
    public String getEnterprise() {
        return Objects.requireNonNull(entepriseEditText.getText()).toString();
    }

    @Override
    public String getDepartment() {
        return Objects.requireNonNull(departmentEditText.getText()).toString();
    }

    @Override
    public String getSirenCode() {
        return Objects.requireNonNull(sirenCodeEditText.getText()).toString();
    }

    @Override
    public String getCity() {
        return Objects.requireNonNull(cityTextView.getText()).toString();
    }

    @Override
    public void setCitySuggestions(List<String> citySuggestions) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), com.google.android.material.R.layout.support_simple_spinner_dropdown_item, citySuggestions);
        cityTextView.setAdapter(adapter);
    }

    @Override
    public void updateNextButtonState(boolean isEnabled) {
        nextButton.setEnabled(isEnabled);
    }

    @Override
    public void passDataToActivity(String enterprise, String department, String siren_code, String city) {
        onRegistrationButtonListener.onEmployerFirstButtonClick(enterprise, department, siren_code, city);
    }
}