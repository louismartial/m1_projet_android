package com.android.gestion_interim.job_search;

import com.android.gestion_interim.network.JobSummaryResponse;
import com.android.gestion_interim.network.JobTitleResponse;

import java.util.List;

public interface JobSearchView {
    void showTitleJobs(List<JobTitleResponse> jobList);
    void showSummaryJobs(List<JobSummaryResponse> jobList);
    void clearJobs();
}