package com.android.gestion_interim.employer.publication;

public interface PublicationView {
    void moveToJobOfferDetailsExtraFragment();

    void onPublicationSuccess();
    void onPublicationFailure();
}
