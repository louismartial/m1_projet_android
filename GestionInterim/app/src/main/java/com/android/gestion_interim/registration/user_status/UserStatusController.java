package com.android.gestion_interim.registration.user_status;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.android.gestion_interim.R;

public class UserStatusController {
    private final UserStatusView view;

    public UserStatusController(UserStatusView view) {
        this.view = view;
    }

    public void setUserStatusOptions(Context context) {
        String[] options = context.getResources().getStringArray(R.array.user_status_options);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, com.google.android.material.R.layout.support_simple_spinner_dropdown_item, options);

        view.setUserStatusAdapter(adapter);
    }

    public void updateLoginButtonState() {
        view.updateLoginButtonState();
    }

    public void onNextButtonClick() {
        String userStatus = view.getUserStatus();

        view.passDataToActivity(userStatus);
    }
}
