package com.android.gestion_interim.registration.user_status;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.gestion_interim.R;
import com.android.gestion_interim.registration.OnRegistrationButtonListener;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;

public class UserStatusFragment extends Fragment implements UserStatusView {
    private MaterialAutoCompleteTextView userStatusTextView;
    private MaterialButton nextButton;

    private OnRegistrationButtonListener onRegistrationButtonListener;

    private UserStatusController controller;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnRegistrationButtonListener) {
            onRegistrationButtonListener = (OnRegistrationButtonListener) context;
        } else {
            throw new ClassCastException(context + " DOIT IMPLÉMENTER OnRegistrationButtonListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onRegistrationButtonListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_user_status, container, false);

        userStatusTextView = view.findViewById(R.id.text_view_user_status);
        nextButton = view.findViewById(R.id.button_next);

        controller = new UserStatusController(this);

        controller.setUserStatusOptions(container.getContext());
        userStatusTextView.setOnItemClickListener((adapterView, view1, i, l) -> controller.updateLoginButtonState());
        nextButton.setOnClickListener(v -> controller.onNextButtonClick());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        controller.setUserStatusOptions(requireContext());
    }

    @Override
    public String getUserStatus() {
        return userStatusTextView.getText().toString();
    }

    @Override
    public void setUserStatusAdapter(ArrayAdapter<String> adapter) {
        userStatusTextView.setAdapter(adapter);
    }

    @Override
    public void updateLoginButtonState() {
        nextButton.setEnabled(true);
    }

    @Override
    public void passDataToActivity(String userStatus) {
        onRegistrationButtonListener.onUserStatusButtonClick(userStatus);
    }
}