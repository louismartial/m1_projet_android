package com.android.gestion_interim.job_search;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.gestion_interim.R;
import com.android.gestion_interim.employer.job_offers.JobSummaryAdapter;
import com.android.gestion_interim.network.JobSummaryResponse;
import com.android.gestion_interim.network.JobTitleResponse;

import java.util.ArrayList;
import java.util.List;

public class JobSearchFragment extends Fragment implements JobSearchView {
    private final List<JobTitleResponse> jobs = new ArrayList<>();
    private final Handler searchHandler = new Handler(Looper.getMainLooper());
    private Runnable searchRunnable;
    private JobTitleAdapter adapter;
    private JobSearchController controller;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job_search, container, false);

        SearchView searchView = view.findViewById(R.id.search_view);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);


        adapter = new JobTitleAdapter(jobs);
        adapter.setOnItemClickListener(job -> searchView.setQuery(job.getTitle(), false));

        recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        recyclerView.setAdapter(adapter);

        controller = new JobSearchController(this);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String keywords) {
                if (!TextUtils.isEmpty(keywords)) {
                    controller.searchJobOffers(keywords);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (searchRunnable != null) {
                    searchHandler.removeCallbacks(searchRunnable);
                }
                searchRunnable = () -> {
                    if (!TextUtils.isEmpty(newText)) {
                        controller.searchTitleJobs(newText);
                    } else {
                        controller.clearSearch();
                    }
                };
                searchHandler.postDelayed(searchRunnable, 256);
                return false;
            }
        });
        return view;
    }

    @Override
    public void showTitleJobs(List<JobTitleResponse> jobList) {
        jobs.clear();
        jobs.addAll(jobList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showSummaryJobs(List<JobSummaryResponse> jobList) {
        clearJobs();
        RecyclerView recyclerView = getView().findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        JobSummaryAdapter adapter = new JobSummaryAdapter(jobList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void clearJobs() {
        jobs.clear();
        adapter.notifyDataSetChanged();
    }
}