package com.android.gestion_interim.job_search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.gestion_interim.R;
import com.android.gestion_interim.network.JobTitleResponse;

import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class JobTitleAdapter extends RecyclerView.Adapter<JobTitleAdapter.ViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(JobTitleResponse job);
    }

    private final List<JobTitleResponse> jobs;
    private OnItemClickListener listener;

    public JobTitleAdapter(List<JobTitleResponse> jobs) {
        this.jobs = jobs;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup group, int viewType) {
        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_job_search, group, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JobTitleResponse job = jobs.get(position);
        holder.titleTextView.setText(job.getTitle());
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MaterialTextView titleTextView;

        public ViewHolder(View view) {
            super(view);
            titleTextView = view.findViewById(R.id.text_view_title);

            itemView.setOnClickListener(v -> {
                if (listener != null && getAdapterPosition() != RecyclerView.NO_POSITION) {
                    listener.onItemClick(jobs.get(getAdapterPosition()));
                }
            });
        }
    }
}

