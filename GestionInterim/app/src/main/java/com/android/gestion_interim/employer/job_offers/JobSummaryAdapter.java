package com.android.gestion_interim.employer.job_offers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.gestion_interim.R;
import com.android.gestion_interim.network.JobSummaryResponse;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class JobSummaryAdapter extends RecyclerView.Adapter<JobSummaryAdapter.ViewHolder> {
    private final List<JobSummaryResponse> jobs;

    public JobSummaryAdapter(List<JobSummaryResponse> jobs) {
        this.jobs = jobs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tile_job_offer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JobSummaryResponse job = jobs.get(position);
        holder.titleTextView.setText(job.getTitle());
        holder.startingDateTextView.setText(job.getStartingDate());
        holder.cityTextView.setText(job.getCity());
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public MaterialTextView titleTextView;
        public MaterialTextView startingDateTextView;
        public MaterialTextView cityTextView;

        public ViewHolder(View view) {
            super(view);
            titleTextView = view.findViewById(R.id.text_view_title);
            cityTextView = view.findViewById(R.id.text_view_city);
            startingDateTextView = view.findViewById(R.id.text_view_starting_date);
        }
    }
}