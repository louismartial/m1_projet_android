package com.android.gestion_interim.employer.publication.job_offer_details_extra;

public interface JobOfferDetailsExtraView {
    String getPurpose();
    String getProfile();

    void updatePublicationButtonState(boolean isEnabled);

    void passDataToActivity(String purpose, String profile);
}
