package com.android.gestion_interim.registration.user_status;

import android.widget.ArrayAdapter;

public interface UserStatusView {
    String getUserStatus();

    void setUserStatusAdapter(ArrayAdapter<String> adapter);
    void updateLoginButtonState();
    void passDataToActivity(String userStatus);
}
